/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   WriteAndRead.java                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbaagman <tbaagman@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/03 16:04:07 by tbaagman          #+#    #+#             */
/*   Updated: 2019/07/06 12:10:20 by tbaagman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

package za.wethinkcode.simulator;

import java.io.*;
import java.util.logging.Logger;
import java.util.logging.Level;

import za.wethinkcode.aircraft.AircraftFactory;
import za.wethinkcode.aircraft.Flyable;
import za.wethinkcode.tower.WeatherTower;

public class WriteAndRead {

	private static final Logger logger = Logger.getLogger(WriteAndRead.class.getName());

	private WeatherTower weatherTower;

	public void writeMessageToFile(String message) {
		try {
			Simulator.getBufferedWriter().write(message);
			Simulator.getBufferedWriter().newLine();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "There was an error while writing to simulation file");
		}
	}

	public int readFileRegisterFlyable(File file, AircraftFactory aircraftFactory) {

		int lineNumber = 1;
		int simulationRuns = 0;
		String lineRead;
		String[] splitParams;
		try (BufferedReader buff = new BufferedReader(new FileReader(file))) {
			while ((lineRead = buff.readLine()) != null) {
				if (lineNumber == 1) {
					simulationRuns = Integer.parseInt(lineRead);
					if (simulationRuns < 0) {
						logger.log(Level.SEVERE, "First Line of scenario file must be a positive integer..");
						return 0;
					}
				} else {
					splitParams = lineRead.split(" ");
					if ((splitParams.length == 1) && (splitParams[0].isEmpty()))
						continue;
					registerToTower(splitParams, aircraftFactory);
				}
				lineNumber++;
			}
		} catch (IOException exception) {
			logger.log(Level.SEVERE, "Cannot read from scenario file.");
			return 0;
		} catch (NumberFormatException numberFormatException) {
			logger.log(Level.INFO, "Parameters 3 to 5 must be integers.");
			return 0;
		}
		return simulationRuns;
	}

	private void registerToTower(String[] splitParams, AircraftFactory aircraftFactory) {
		Flyable flyable;

		if (splitParams.length == 5) {
			flyable = aircraftFactory.newAircraft(
					splitParams[0],
					splitParams[1],
					Integer.parseInt(splitParams[2]),
					Integer.parseInt(splitParams[3]),
					Integer.parseInt(splitParams[4]));
			if (flyable != null) {
				flyable.registerTower(getWeatherTower());
			} else {
				logger.log(Level.SEVERE, "Unknown Aircraft type.");
			}
		} else {
			logger.log(Level.INFO, "Each Line after the first Line must have 5 parameters separated by a single space...");
		}
	}

	public WeatherTower getWeatherTower() {
		return this.weatherTower;
	}

	public void setWeatherTower(WeatherTower weatherTower) {
		this.weatherTower = weatherTower;
	}
}
