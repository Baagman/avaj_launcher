/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Simulator.java                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbaagman <tbaagman@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/20 12:17:55 by tbaagman          #+#    #+#             */
/*   Updated: 2019/07/06 11:34:42 by tbaagman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

package za.wethinkcode.simulator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import za.wethinkcode.aircraft.AircraftFactory;
import za.wethinkcode.tower.WeatherTower;

public class Simulator {

    private static final Logger logger = Logger.getLogger(Simulator.class.getName());

    private static BufferedWriter bufferedWriter;

    public static void main(String[] args) {
        if (args.length == 1) {

            File scenarioFile = new File(args[0]);

            if ((scenarioFile.exists()) && (!scenarioFile.isDirectory())) {
                try {
                    simulateWeather(scenarioFile);
                } catch (IOException e) {
                    logger.log(Level.INFO, "Cannot create output file...");
                }
            } else {
                logger.log(Level.INFO, "Specified file does not exist");
            }
        } else {
            logger.log(Level.INFO, "Please specify file");
        }
    }

    private static void simulateWeather(File scenarioFile) throws IOException {

        File simulationFile = new File("simulation.txt");
        bufferedWriter = new BufferedWriter(new FileWriter(simulationFile));
        WriteAndRead writeAndRead = new WriteAndRead();
        WeatherTower tower = new WeatherTower();

        writeAndRead.setWeatherTower(tower);
        int simulationRuns = writeAndRead.readFileRegisterFlyable(scenarioFile, new AircraftFactory());

        if (simulationRuns > 0) {
            while (simulationRuns > 0) {
                tower.changeWeather();
                simulationRuns--;
            }
        } else {
            simulationFile.deleteOnExit();
        }
        bufferedWriter.close();
    }

    public static BufferedWriter getBufferedWriter() {
        return bufferedWriter;
    }

}
