/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   WeatherProvider.java                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbaagman <tbaagman@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/27 13:35:02 by tbaagman          #+#    #+#             */
/*   Updated: 2019/07/04 15:00:31 by tbaagman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

package za.wethinkcode.weather;
import za.wethinkcode.coordinates.Coordinates;

import java.util.Random;

public class WeatherProvider {
	
	private static final WeatherProvider weatherProvider = new WeatherProvider();
	private static final Random random = new Random();
	private static final String[] weather = {
		"SNOW",
		"SUN",
		"RAIN",
		"FOG"
	};

	public static WeatherProvider getProvider() {
		return WeatherProvider.weatherProvider;
	}

	public String getCurrentWeather(Coordinates coordinates) {
		int weatherSeed = random.nextInt(4);
		return weather[weatherSeed % 4];
	}
}